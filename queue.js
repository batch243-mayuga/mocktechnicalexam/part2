let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection;
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
   collection[collection.length] = element;
   return collection;
   console.log(collection);
}

function dequeue() {
    // In here you are going to remove the first element in the array

    for(let i = 0; i < collection.length - 1; i++){
        collection[i] = collection[i + 1];
    }
    --collection.length;
    return collection;
    console.log(collection);
}

function front() {
    // In here, you are going to capture the first element
    let item = collection[0];
    return item;
    console.log(item);
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements 
     let total = 0;
     for(i in collection){
        total++;
     }
     return total;
}

function isEmpty() {
    //it will check whether the function is empty or not
    if(collection > 0){
        return true;
    } else {
        return false;
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};